class Dictionary {
  /** @type {DictionaryEntry[]} */
  entries;

  constructor(entries) {
    this.entries = entries;
  }

  /** @return {Promise<Dictionary>} */
  static async load() {
    /** @type {[string, string][]} */
    const dictionary = await (await fetch("wortliste.json")).json();

    return new Dictionary(
      dictionary.map(([a, b], id) => ({
        id,
        a,
        a_raw: a.toLowerCase(),
        b,
        b_raw: b.toLowerCase(),
      })),
    );
  }

  /**
   * @return {HTMLTableRowElement[]}
   */
  all() {
    return this.entries.map(({ a, b }) => {
      let tr = document.createElement("tr");
      tr.appendChild(highlight(a, -1));
      tr.appendChild(highlight(b, -1));
      return tr;
    });
  }

  /**
   * @param {string} pattern
   * @return {HTMLTableRowElement[]}
   */
  search(pattern) {
    return this.entries
      .map(({ a, a_raw, b, b_raw }) => {
        let a_index = a_raw.indexOf(pattern);
        let b_index = b_raw.indexOf(pattern);

        if (a_index < 0 && b_index < 0) {
          return null;
        } else {
          let tr = document.createElement("tr");
          tr.appendChild(highlight(a, a_index, pattern.length));
          tr.appendChild(highlight(b, b_index, pattern.length));
          return tr;
        }
      })
      .filter((tr) => tr != null);
  }
}

/**
 * @typedef DictionaryEntry
 * @prop {number} id
 * @prop {string} a
 * @prop {string} a_raw
 * @prop {string} b
 * @prop {string} b_raw
 */

/**
 * @param {string} s
 * @param {number} start
 * @param {number} len
 * @param {string} s
 * @return {HTMLTableCellElement}
 */
function highlight(s, start, len) {
  let td = document.createElement("td");

  if (start < 0) {
    td.appendChild(document.createTextNode(s));
    return td;
  }

  td.appendChild(document.createTextNode(s.slice(0, start)));

  let mark = document.createElement("mark");
  mark.appendChild(document.createTextNode(s.slice(start, start + len)));
  td.appendChild(mark);

  td.appendChild(document.createTextNode(s.slice(start + len)));

  return td;
}

const dictionary = await Dictionary.load();

if (document.readyState !== "loading") {
  init();
} else {
  document.addEventListener("DOMContentLoaded", init);
}

function init() {
  /** @type {HTMLInputElement} */
  const searchField = document.getElementById("search");
  const resultsTable = document.getElementById("results");

  let query = location.hash.slice(1);
  searchField.value = query;
  showSearchResults(query);

  searchField.addEventListener("input", (ev) => {
    /** @type {string} */
    let query = ev.target.value;
    location.replace(`#${query}`);
    showSearchResults(query);
  });

  /**
   * @param {string} query
   */
  function showSearchResults(query) {
    let pattern = query.toLowerCase();

    let results = pattern ? dictionary.search(pattern) : dictionary.all();

    resultsTable.replaceChildren(...results);
  }
}
