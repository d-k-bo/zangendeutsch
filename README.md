# Zangendeutsch-Wörterbuch

Eine kleine Übersicht von Begriffen, die in den ich_iel-Kommunen auf [Feddit](https://feddit.de/c/ich_iel) und [Reddit](https://reddit.com/r/ich_iel) zur Anwendung kommen.

## Zug-Anfragen willkommen!

Zum Ergänzen der Wortliste muss nur `wortliste.json` ergänzt werden. Dabei bitte

- alphabetische Sortierung berücksichtigen
- wenn der ursprünglische Begriff im Deutschen üblich ist, die eingedeutsche Schreibweise wählen
- Abkürzungen bzw. Akronyme mit einer Erklärung in Klammern versehen

Bitte nur Begriffe hinzufügen, die auch tatsächlich verwendet werden.